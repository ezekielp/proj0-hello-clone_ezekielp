# Proj0-Hello
-------------

Author: Zeke Petersen

https://bitbucket.org/ezekielp/proj0-hello-clone_ezekielp

Trivial project to exercise version control, turn-in, and other
mechanisms. Prints "Hello world".

